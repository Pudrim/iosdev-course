/*

Optional in Swift

*/

var rg:Int = 12121212
var driverLicense:Int = 22222333333
print("A minha carteira de motorista é:",driverLicense)

//declara a variavel sem atribuir nada
var newDriverLicense:Int?
if let canDriver = newDriverLicense{
  print("Minha carteira é:\(newDriverLicense),eu posso dirigir!")
} else{
  "Eu não tenho carteira de motorista, não posso dirigir!"
}

var number:String = "578"
//Int(number)-> convert para inteiro e tenta desembrulhar a varavel number, se não conseguir, retorna 0.
let addressNumber = Int(number) ?? 0
print(addressNumber)

var name:String? = nil
if name == nil{
    name = "teste"
    print(name!)
    //retoorno: print(name)
    // print(name) retorno: Optional("teste")
}else{
    print(name)
}
