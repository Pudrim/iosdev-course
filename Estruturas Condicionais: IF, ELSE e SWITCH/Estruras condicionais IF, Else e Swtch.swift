/*

Operadores logicos com swfit

*/
//Estruturas condicionais IF, ELSE e Switch

var hasHeadache:Bool = false
var humor:String = ""

//Se o valor de hasHeadache for true, vai retornar "estou bolado"
if hasHeadache{
	humor = "estou bolado"
	print("humor")
}else{
	humor = "Felizão"
	print("humor")
}

let number = 11
if number % 2 ==0 {
	print("O número é par"
}else{
print("Ele é ímpar")
}


var temperature = 10
var climate = ""
 
if temperature <= 0{
	climate = "Frio pra caramba."
	print(climate)
}else if temperature < 14 {
	climate = "Está frio."
	print(climate)
}else if temperature < 21 {
	climate = "Clima agradavel."
	print(climate)
}else if temerature < 30 {
	climate = "Um pouco quente."
	print(climate)	
}else {
	climate = "Muito quente."
	print(climate)
}

let letter = "e"
var letterType = ""

if letter == "a" || letter == "e" || letter == "i" || letter == "o" || letter == "u"{
	letterType = "vogal"
	print(letterType)
}else{
	letterType = "consoante"
	print(letterType)
}

//Swith

let letter = "e"
var letterType = ""

switch letter{
	case "a":
		letterType = "vogal"
		print(letterType)
	case "e":
		letterType = "vogal"
		print(letterType)
	case "i":
		letterType = "vogal"
		print(letterType)
	case "o":
		letterType = "vogal"
		print(letterType)
	case "u":
		letterType = "vogal"
		print(letterType)
	default:
		letterType = "consoante"
		print(letterType)
}
	
//Switch simplificado

let letter = "e"
var letterType = ""

switch letter{
	case "a","e","i","o","u":
		letterType = "vogal"
		print(letterType)
	default:
		letterType = "consoante"
		print(letterType)
}

let speed = 95.0
switch myspeed{
    case myspeed 0.0..<96.0:
        print("Primeira Marcha")
    default:
        print("Quinta Marcha")
}
