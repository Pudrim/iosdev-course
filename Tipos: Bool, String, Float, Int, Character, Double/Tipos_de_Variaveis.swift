/*
Tipos de variáveis
em Swift
*/

//Booleanos - Verdadeiro/falso
var IsFristTime:Bool = true
print(IsFristTime)

var LikesFruit:Bool = true
print(LikesFruit)

//inteiro (Int)
var age:Int = -25
print(age)
 //ou
var newAge = 35
print(newAge)

//inteiros positivos ou Unsingned int
var newAgeTipe:UInt = 37 
print(newAgeTipe)

//Float -> decimal com poucos digitos 32bits
var dolarPrice:Float = 3.55
print(dolarPrice)

//Double 64bits
let crazyNumber = 1000000000.79983334522
print(crazyNumber)

let NewCrazyNumber:Double = 9999999999.88888885563
print(NewCrazyNumber)

//Character: 1 caracter
var newGender:Character = "M"
print(newGender)

//pular 1 linha na exibição
var enter:Character = "\n"
print(enter)

//repil.it não reconheceu este comando
//var aspas:Character = "\""
//print("\")

//let studentName = "\"Dr\" Thiago Silva"
//print(studentName)

//declaração de constante
let firstName = "Thiago"
let lastName = "Silva"
let fullName = firstName + " " + lastName
print(fullName)

//ou
let newFullName = "\(firstName) \(lastName) - Idade: \(newAge) anos"
print(newFullName)

//repl.it não reconheceu este comando
//let text = ***Olá, 
//                  tudo bem?***
//print(text)