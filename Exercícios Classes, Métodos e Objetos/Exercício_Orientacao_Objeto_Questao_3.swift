/* Escreva uma classe em que cada objeto representa um vôo que acontece em determinada data e em
determinado horário. Cada vôo possui no máximo 100 passageiros, e a classe permite controlar a ocupação
das vagas. A classe deve ter os seguintes métodos:


construtor -> configura os dados do vôo (recebidos como parâmetro): número do vôo, data (para
armazenar a data utilize um objeto da classe Data, criada na questão anterior);
proximoLivre -> retorna o número da próxima cadeira livre
verifica -> verifica se o número da cadeira recebido como parâmetro está ocupada
ocupa -> ocupa determinada cadeira do vôo, cujo número é recebido como parâmetro, e retorna
verdadeiro se a cadeira ainda não estiver ocupada (operação foi bem sucedida) e falso caso contrário
vagas -> retorna o número de cadeiras vagas disponíveis (não ocupadas) no vôo
getVoo -> retorna o número do vôo

*/

class Voo{
	var numeroVoo: Int
	var data: String
	var assentos: [Int] = []
	
	init(numeroVoo: Int, data: String){
		self.numeroVoo = numeroVoo
		self.data = data
	}
	
	func consultarProximoLivre(){
    	let proximoLivre = self.assentos.count + 1
    	print("O proximo assento livre é o assento \(proximoLivre)!")
	}
	
	func verificar(numeroCadeiraDesejado:Int)-> String{
		if numeroCadeiraDesejado > 100{
			print("Só temos assentos do 1 ao 100")
			let status = "Ocupada"
			return status
		} else {
			if self.assentos.contains(numeroCadeiraDesejado){
				print("Assento \(numeroCadeiraDesejado) está ocupado!")
				let status = "Ocupada"
				return status
			} else {
				print("Assento \(numeroCadeiraDesejado) está livre!")
				let status = "Livre"
				return status     
			}
		}
	}

	func ocupar(numeroCadeiraDesejado:Int)-> [Int]{
		let cadeiraLivre = verificar(numeroCadeiraDesejado:numeroCadeiraDesejado)
		if cadeiraLivre == "Livre"{
			self.assentos.append(numeroCadeiraDesejado)
			print("Assento \(numeroCadeiraDesejado) foi ocupado!")
			return self.assentos
		} else{
			print("A cadeira \(numeroCadeiraDesejado) está ocupada, por favor escolha outra cadeira!")
			return self.assentos
		}
				
	}

	func consultarVagas(capacidadeVoo:Int)-> Int{
		let vagasDisponiveis = capacidadeVoo - self.assentos.count 
		print("Temos \(vagasDisponiveis) vagas disponíveis para  este vôo")
    	return vagasDisponiveis
	}
	
	func obterNumeroVoo()-> Int{
		let numeroVoo = self.numeroVoo
    	print("O número do vôo é: \(numeroVoo)")
		return numeroVoo
	}

	func obterDataVoo()-> String{
		let dataVoo = self.data
    	print("O número do vôo é: \(dataVoo)")
		return dataVoo
	}
}

let voo = Voo(numeroVoo:6677,data:"13/06/2020 20:15:00")
let numeroVoo = voo.obterNumeroVoo()
let dataVoo = voo.obterDataVoo()
print("\n")

let vagaLivre = voo.consultarVagas(capacidadeVoo:100)
let assento = voo.ocupar(numeroCadeiraDesejado:1)
voo.consultarProximoLivre()
print("\n")


let vagaLivreOne = voo.consultarVagas(capacidadeVoo:100)
let assentoOne = voo.ocupar(numeroCadeiraDesejado:2)
voo.consultarProximoLivre()
print("\n")

let vagaLivreTwo = voo.consultarVagas(capacidadeVoo:100)
let assentoTwo = voo.ocupar(numeroCadeiraDesejado:101)
voo.consultarProximoLivre()
print("\n")

/*
output ->

O número do vôo é: 6677


Temos 100 vagas disponíveis para  este vôo
Assento 1 está livre!
Assento 1 foi ocupado!
O proximo assento livre é o assento 2!


Temos 99 vagas disponíveis para  este vôo
Assento 2 está livre!
Assento 2 foi ocupado!
O proximo assento livre é o assento 3!


Temos 98 vagas disponíveis para  este vôo
Assento 1 está ocupado!
A cadeira 1 está ocupada, por favor escolha outra cadeira!
O proximo assento livre é o assento 3!

*/
