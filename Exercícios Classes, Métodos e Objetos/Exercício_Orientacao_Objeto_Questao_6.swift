/*

Cria uma classe VetorOrdenado que é uma variante da classe anterior, com as seguintes modificações
e acréscimos:
insert -> recebe uma string como parâmetro e a insere em uma posição tal que o vetor de nomes se
mantenha sempre em ordem alfabética.
merge -> recebe como parâmetro um outro objeto da classe VetorOrdenado e retorna um terceiro objeto
da classe VetorOrdenado (criado dentro do método) que contém o merge do vetor corrente com
aquele recebido como parâmetro

*/

class Vector {
    var vectorSize:Int
    var vector:[String] = []

    init(vectorSize:Int){
        self.vectorSize = vectorSize
    }

    func insertVector(_ insertStringVector:String){
        var insertString = self.vector.append(insertStringVector)
        print("O vetor atual é \(self.vector)")   
    }

    func sortVector(){
      var vectorSorted = self.vector.sorted()
      print("O vetor ordenado é \(vectorSorted)")
    } 

    func mergeVector(_ vectorCreated:[String]){
        var vectorA = self.vector
        var vectorB = vectorCreated
        var vectorMerged = (vectorA + vectorB).sorted()
        print("A união do vetor \(vectorA) com o vetor \(vectorB)em ordem crescente é \(vectorMerged)")

    }
}

var vectorAnalisys = Vector(vectorSize:4)
var insertVector  = vectorAnalisys.insertVector("B")
var insertVector1 = vectorAnalisys.insertVector("A")
var teste = (vectorAnalisys.sortVector())

let vectorCreated = ["C","D"]
var vectorMerged = vectorAnalisys.mergeVector(vectorCreated)
