class Aluno {
    
    var nome: String
    var matricula: String
    var notasDeProva: [Double]
    var notasDeTrabalho: [Double]
    let minimoParaAprovacao = 7.0
    
    init(nome: String, matricula: String, notasDeProva: [Double], notasDeTrabalho: [Double]) {
        self.nome = nome
        self.matricula = matricula
        self.notasDeProva = notasDeProva
        self.notasDeTrabalho = notasDeTrabalho
    }
    
    func obterMediaAtual() -> Double {
        let notaFinalPrimeiraProva = self.obterNotaFinal(peso: 2.5, nota: self.notasDeProva[0])
        let notaFinalSegundaProva = self.obterNotaFinal(peso: 2.5, nota: self.notasDeProva[1])
        let notaFinalTrabalho = self.obterNotaFinal(peso: 2.0, nota: self.notasDeTrabalho[0])
        return notaFinalPrimeiraProva + notaFinalSegundaProva + notaFinalTrabalho
    }
    
    func obterNotaFinal(peso: Double, nota: Double) -> Double {
        return (peso / 10) * nota
    }
    
    func calcularQuantoFaltaParaAprovacao() -> Double {
        let mediaAtual = self.obterMediaAtual()
        let faltaParaAprovacao = self.minimoParaAprovacao - mediaAtual
        if faltaParaAprovacao <= 0 {
            print("O aluno \(self.nome) tem média \(mediaAtual), foi aprovado!")
        } else {
            print("O aluno \(self.nome) tem média \(mediaAtual), falta \(faltaParaAprovacao) para ser aprovado!")
        }
        return faltaParaAprovacao
    }
    
    func alterarNome(_ novoNome: String) {
        self.nome = novoNome
    }
    
    func alterarMatricula(_ novaMatricula: String) {
        self.matricula = novaMatricula
    }
}
