/*

Considere a seguinte classe, cujo método respostaQuestao recebe como parâmetro o número de uma
questão e retorna a sua resposta correta, proveniente de um gabarito.
public class Gabarito
{
  public char respostaQuestao(int numeroQuestao)
  {
    :
  }
}

Escreva uma classe classe Prova em que cada objeto representa uma prova feita por um aluno. Esta
prova possui 15 questões de múltipla escolha (letras A a E). As 10 primeiras questões valem 0,5 ponto e as
5 últimas questões valem 1 ponto. Esta classe deverá controlar as questões respondidas pelo aluno. Para
isto, a classe deve implementar os métodos:

construtor -> recebe como parâmetro um objeto da classe Gabarito contendo o gabarito da prova
respostaAluno -> recebe como parâmetro a resposta dada pelo aluno a uma questão; este método não
recebe entre os parâmetros o número da questão, ele mesmo deve estabelecer um
controle interno de modo que as questões sejam inseridas sequencialmente, ou seja, a
primeira vez que o método é chamado, insere a primeira questão, a segunda, insere a
segunda questão, e assim por diante.
acertos -> retorna a quantidade de questões que o aluno acertou
nota -> retorna a nota que o aluno tirou na prova
maior -> recebe como parâmetro um outro objeto da classe Prova e retorna a nota do aluno que
acertou mais questões; se houver empate, retorna a maior nota; se

*/

class ListOfAnswers{

  //var rightAnswer = [1:"A",2:"B",3:"C",4:"D",5:"E",6:"A",7:"B",8:"C",9:"D",10:"E",11:"A",12:"B",13:"C",14:"D",15:"E"]
  
  /*
  func checkRightAnswer(numberQuestion:Int)-> String{
    if numberQuestion >= 0 && numberQuestion < 15 {
      let responseCorrect = rightAnswer[numberQuestion]!
      return responseCorrect
    } else {
      return "Numero de questão inválido"
    }
  } 
  */

  var rightAnswer = ["A","B","C","D","E","A","B","C","D","E","A","B","C","D","E"]

  func checkRightAnswer(numberQuestion:Int)-> String{
    let numberQuestion = numberQuestion - 1
    if numberQuestion >= 0 && numberQuestion < 15 {
      let response =  self.rightAnswer[numberQuestion]!
      return response
    } else{
      return "Numero de questão inválido"
    }
  }

}

let result = ListOfAnswers()
print(result.checkRightAnswer(numberQuestion:15))

//output -> E

class SchoolTest{

  var alunos:[String]
  var hits:[String] = []
  var questionAnswer:[String] = [] 
  var hanking:[Double] = []
  let instanceCheck = ListOfAnswers()
  let checkQuestion = instanceCheck.rightAnswer 

  init(check:String){
    self.alunos = alunos
    super.init()

  }

  func checkPoints(numberQuestion:Int)-> Int {
    switch numberQuestion {
      case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,:
        self.hits = 0.5
        return self.hit
      default:
        self.hits = 1.0
        return self.hits
    }
  }

  //armazenar as respostas do aluno na variavel questionAnswer
  func getReponse(answer:String){ //recebe a resposta do aluno e armazena na variavel questionAnswer [A,B,D,C,E]
    self.questionAnswer.append(answer) 
  }

  //retornar a quantidade de questoes acertadas pelo aluno
  func getHits(questionAnswerList:[String])-> Int{ 
    //retorna true para acerto e contar os true dentro da lista, deve retornar a quantidade de acertos do aluno  questionAnswerList = [A,B,D,C,E]
    let index = self.questionAnswerList
    for a in 0....index.count -1{
      let reponse = (rightAnswer[a]) //resposta do aluno
      let proof = self.checkQuestion[a] //gabarito
      if reponse == proof{
        self.hits.append("true")
      } else {
        self.hits.append("false")
      }
    }
    var countTrue = 0
    for counts in self.hits{
      if counts == "true"{
        countTrue += 1
      }
    print ("O Aluno Acertou \(countTrue) questões!")
    }   
  } return countTrue

  func getScore()-> Int{ //deve retornar a nota do aluno
    let score = checkPoints(numberQuestion)
    let index = self.questionAnswerList
    
    for a in 0....index.count -1{
      let reponse = (rightAnswer[a])
      let proof = checkQuestion[a]
      if reponse == proof{
        
      } else {
        self.hits.append("false")
      }
    if 
    self.hits.append(score)
  }

  func sumScore()-> Int{ //deve retornar a nota do aluno
    var result = 0
    for a in self.questionAnswer {
      result += a
    return result
  }

  func getFirst(){ //deve retornar o aluno com melhor nota
    
  }
}

