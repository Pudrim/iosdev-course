/*
QUESTÃO 1: Escreva uma classe cujos objetos representam alunos matriculados em uma disciplina. Cada objeto
dessa classe deve guardar os seguintes dados do aluno: matrícula, nome, 2 notas de prova e 1 nota de
trabalho. Escreva os seguintes métodos para esta classe:
media -> calcula a média final do aluno (cada prova tem peso 2,5 e o trabalho tem peso 2)
final -> calcula quanto o aluno precisa para a prova final (retorna zero se ele não for para a final)

Segunda parte  -> construir um método que altere o nome do aluno e sua matricula

*/
class AlunoMatriculado{
    var name:String
    var matricula:String
    var nota:[Double] = []
    var trabalho:[Double] = []

    init(name:String, matricula:String){
        self.name = name
        self.matricula = matricula 
    }
    func calcularNotaMedia()-> Double{
      let peso1 = (2.5 / 10) * nota[0]
      let peso2 = (2.5 / 10) * nota[1]
      let peso3 = (2.0 / 10) * trabalho[0]
      let notaGeral = peso1 + peso2 + peso3
      return notaGeral
    }

    func calcularFaltaParaPassar(_ meta:Double )-> Double{
      let notaGeral = calcularNotaMedia()
      if notaGeral == meta {
        print("O aluno \(self.name) tem média \(notaGeral), foi aprovado!")
        return 0
      } else {
        let faltaParaPassar = meta - notaGeral
        print("O aluno \(self.name) tem média \(notaGeral), falta \(faltaParaPassar) para ser aprovado!")
        return faltaParaPassar
      }   
    }

    func alterarNomeAluno(_ novoNome:String){
      self.name = novoNome   
    }
    func alterarMatriculaAluno(_ novaMatricula:String){
      self.matricula = novaMatricula
    }
}
var aluno = AlunoMatriculado(name:"João",matricula:"123456A")
aluno.nota = [10, 10]
aluno.trabalho = [10]
aluno.calcularFaltaParaPassar(7.0)

print("\nNome do aluno e matricula antes da alteraçao", aluno.name, aluno.matricula)

//output = O aluno João tem média 7.0, foi aprovado!
//output = O aluno João tem média 6.75, falta 0.25 para ser aprovado!

aluno.alterarNomeAluno("Thiago")
aluno.alterarMatriculaAluno("111111A")
print("\nNome do aluno e matricula após da alteraçao", aluno.name, aluno.matricula)
