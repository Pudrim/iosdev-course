
/*
A linguagem Java dispõe de um suporte nativo a vetores, que exige a definição de seu tamanho no
momento da instanciação. Depois de instanciado, o tamanho do vetor não pode ser modificado. Escreva
uma classe chamada Vetor cujos objetos simulem vetores de tamanho variável. A classe define os
seguintes métodos:

construtor -> recebe como parâmetro o tamanho inicial do vetor
insert -> recebe como parâmetro uma string e a coloca na próxima posição disponível do vetor; note
que o vetor cresce automaticamente, portanto, se a inserção ultrapassar o tamanho inicial
estabelecido na criação, por exemplo, o vetor deve aumentar seu tamanho
automaticamente
get ->  recebe como parâmetro uma posição do vetor e retorna a string que estiver naquela
posição; se a posição não estiver ocupada ou ultrapassar o tamanho do vetor, este método
retorna nulo
size -> retorna o número de elementos inseridos no vetor (independente do tamanho do mesmo)

O java dispõe de classes – tal como a Vector – que realizam a tarefa solicitada nesta questão. Tais
classes não devem ser usadas. É possível resolver esta questão apenas usando o sistema de vetores
preexistente do Java, sem nenhum comando especial extra.

*/

class Vector {
    var vectorSize:Int
    var vector:[String] = []

    init(vectorSize:Int){
        self.vectorSize = vectorSize
    }

    func insertVector(insertStringVector:String){
        let insertString = self.vector.append(insertStringVector)
        print("O vetor atual é \(self.vector)")
        //return insertString
    }

    func getStringVector(position:Int)-> String{
        if position <= self.vector.count{
            let vectorPosition = self.vector[position - 1]
            print("A letra \(position ) do vetor é: \(vectorPosition)")
            return vectorPosition
        } else {
            return "nil"
        }  
    }
    
    func sizeVector()-> Int{
        if self.vector.count > 1{
            print("O vetor atual tem \(self.vector.count) elementos")
      } else {
          print("O vetor atual tem \(self.vector.count) elemento")      
      }
        return self.vector.count
    } 
    
}

var vectorAnalisys = Vector(vectorSize:0)
var insertVector = vectorAnalisys.insertVector(insertStringVector:"A")
var insertVector1 = vectorAnalisys.insertVector(insertStringVector:"B")
//print(vectorPosition)
var sizeVector = vectorAnalisys.sizeVector()
var getStringVector = vectorAnalisys.getStringVector(position:2)
print(getStringVector)

/*
output -> 
O vetor atual é ["A"]
O vetor atual é ["A", "B"]
O vetor atual tem 2 elementos
A letra 2 do vetor é: B
B

*/





