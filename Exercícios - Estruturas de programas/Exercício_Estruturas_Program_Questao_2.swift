/*

Escreva um programa que leia o nome e salário atual de um funcionário. O programa deve calcular
seu novo salário (segundo a tabela abaixo) e mostrar o nome, o salário atual e o salário reajustado do
funcionário:

    Faixa salarial  
acima de        até         Acréscimo
  ---           150           25%
  150           300           20%
  300           600           15%
  600           ---           10%


-> repita o processo acima até que seja digitado FIM no lugar do nome do funcionário;
-> mostrar ao final do programa a soma dos salários atuais, a soma dos salários reajustados e a
diferença entre eles.

*/

class SalaryAdjustment{

  var employee:String = ""
  var oldSalary:Double
  var newSalary:Double
  var totalOldSalary:[Double] = []
  var totalNewSalary:[Double] = []

  init(oldSalary:Double, newSalary:Double){
    self.oldSalary = oldSalary
    self.newSalary = newSalary
  }

  func getNewSalary(employee:String, oldSalary:Double){
    self.employee = employee
    self.oldSalary = oldSalary
    switch self.oldSalary{
      case 0.0..<151.0:
          self.newSalary = oldSalary * 1.25
          self.totalOldSalary.append(oldSalary)
          self.totalNewSalary.append(self.newSalary)
          print("O colaborador \(self.employee) tem salário de \(self.oldSalary) e será reajustado para \(self.newSalary)")
      case 151.0..<301.0:
          self.newSalary = oldSalary * 1.20
          self.totalOldSalary.append(oldSalary)
          self.totalNewSalary.append(self.newSalary)

          print("O colaborador \(self.employee) tem salário de \(self.oldSalary) e será reajustado para \(self.newSalary)")
      case 301.0..<601.0:
          self.newSalary = oldSalary * 1.15
          self.totalOldSalary.append(oldSalary)
          self.totalNewSalary.append(self.newSalary)
          print("O colaborador \(self.employee) tem salário de \(self.oldSalary) e será reajustado para \(self.newSalary)")
      default:
          self.newSalary = oldSalary * 1.10
          self.totalOldSalary.append(oldSalary)
          self.totalNewSalary.append(self.newSalary)
          print("O colaborador \(self.employee) tem salário de \(self.oldSalary) e será reajustado para \(self.newSalary)")
    }

  }
  
  func calcTotalOldSalary()-> Double{
    var total = 0.0
    for a in self.totalOldSalary {
      total = a + total 
    } 
    print("\nA soma dos salários atuais é: \(total)") 
    return total 
  } 

  func calcTotalNewSalary()-> Double{
    var total = 0.0
    for a in self.totalNewSalary {
      total = a + total 
    } 
    print("A soma dos novos salários é: \(total)")
    return total
  }   
}

var teste = SalaryAdjustment(oldSalary:0, newSalary:0)
var salaryThiago = teste.getNewSalary(employee:"Thiago",oldSalary:300.0)
var salaryTomaz = teste.getNewSalary(employee:"Tomaz",oldSalary:600.0)
var salaryKevin = teste.getNewSalary(employee:"Kevin",oldSalary:500.0)
var sumOldSalary = teste.calcTotalOldSalary()
var sumNewSalary = teste.calcTotalNewSalary()

print("A diferença paga no reajuste será: \(sumNewSalary - sumOldSalary)\nFIM")

/*
Output ->

O colaborador Thiago tem salário de 300.0 e será reajustado para 360.0
O colaborador Tomaz tem salário de 600.0 e será reajustado para 690.0
O colaborador Kevin tem salário de 500.0 e será reajustado para 575.0

A soma dos salários atuais é: 1400.0
A soma dos novos salários é: 1625.0
A diferença paga no reajuste será: 225.0
FIM

*/

