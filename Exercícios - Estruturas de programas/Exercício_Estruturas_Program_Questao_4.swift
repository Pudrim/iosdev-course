/*

Os números de Fibonacci formam uma sequência em que cada número é igual à soma dos dois
anteriores. Os dois primeiros números são, por definição igual a 1, segundo o exemplo abaixo:
Ex: 1 1 2 3 5 8 13 ...
Escreva um programa que carregue um carregue um número inteiro pelo teclado e indique se ele faz
parte da sequência de Fibonacci.

*/


class Fibonacci{
  
    var inputA:String? = nil
    var fiboItensList:[Int] = []
    var convertedA = 0
    var firstNumber = 0
    var secondNumber = 1
    var thirdNumber = 0 
  
    func getInput(){
        if self.inputA == nil{
            print("Digite o número para conferência:")
            self.inputA = readLine()
            self.convertedA = Int(self.inputA!) ?? 0
        if self.convertedA == 0{
            print("O valor digitado não é válido!")
        }else{
            print("\nO valor digitado é: \(self.convertedA)")  
            }
        }
    }

    func getFiboNumbersList(){
        for i in 0..<convertedA + 1 {
            self.thirdNumber = self.firstNumber + self.secondNumber
            self.firstNumber = self.secondNumber
            self.secondNumber = self.thirdNumber
            if self.thirdNumber > self.convertedA{
              self.fiboItensList.append(self.thirdNumber)
              break
            }else{
              self.fiboItensList.append(self.thirdNumber)
            }
              
        }
    }

    func getValidFiboNumber() {
        if self.thirdNumber > self.convertedA{
          let foundNumber = self.fiboItensList.filter { $0 == self.convertedA}
          if foundNumber != [] {
              print("O número \(self.convertedA) pertence a sequência Fiobonacci!")
          }else{
              print("O número \(self.convertedA) não pertence a sequência Fiobonacci!")
              
          }
        }    
         print(self.fiboItensList)
    }           

}

var instance = Fibonacci()
var listItens = instance.getInput()
var test = instance.getFiboNumbersList()
var teste2 = instance.getValidFiboNumber()



/*

output ->

Digite o número para conferência:
50

O valor digitado é: 50
O número 50 não pertence a sequência Fiobonacci!
[1, 2, 3, 5, 8, 13, 21, 34, 55]

*/









