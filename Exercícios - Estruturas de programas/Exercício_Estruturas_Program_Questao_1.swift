/*
Escreva um programa que carregue dois valores A e B pelo teclado e imprima todos os números
ímpares entre A e B.

*/

class OddNumbers{
  
  var input:String? = nil
  var converted:Int = 0
  var listConverted:[Int] = []

  func getInput() {
    print("Digite o número:")
    self.input = readLine()
    self.converted = Int(self.input!) ?? 0
    if self.converted == 0{
      print("O valor digitado não é válido!")
    }else{
      print("O valor digitado é: \(self.converted)\n")
      self.listConverted.append(self.converted) 
    }  
  }

  func getOdds(){ 
    var inputedA = self.listConverted[0]
    var inputedB = self.listConverted[1]
    while  inputedA <= inputedB {
      if inputedA % 2 != 0 {
        print("É ímpar: \(inputedA)")
        inputedA += 1
      }
      inputedA += 1
    }
  }
}

var instance = OddNumbers()
var oddsInputA = instance.getInput()
var oddsInputB = instance.getInput()
var compare = instance.getOdds()


/*
output ->

Digite o número:
1
O valor digitado é: 1

Digite o número:
10
O valor digitado é: 10

É ímpar: 1
É ímpar: 3
É ímpar: 5
É ímpar: 7
É ímpar: 9

*/
