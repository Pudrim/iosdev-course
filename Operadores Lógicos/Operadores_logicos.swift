/*

Operadores logicos com swfit

*/
//Operador E ou AND (&&)
var bougthBanana:Bool = true
print("result bougthBanana is:",bougthBanana)
var boughtTomato:Bool = true
print("result boughtTomato is:",boughtTomato)
var boughtApple = false
print("result boughtApple is:",boughtApple)

var isHappy = bougthBanana && boughtTomato && boughtApple

print("result isHappy is:",isHappy)

true && true
true && false
false && false
false && true

//Operador OU (OR): ||

var likesMeat = false
print("result likesMeat is:",likesMeat)

var likesBeer = false
print("result likesBeer is:",likesBeer)

var canInviteBarbecue = likesBeer || likesMeat
print("result canInviteBarbecue is:",canInviteBarbecue)

var grade1 = 8.5
var grade2 = 7.0
let result2 = (grade1/grade2)/2
print("result2 is:",result2)


//Operador de negação (NOT): !

var konowSwift = true
var enrolSwiftCourse = !konowSwift  //se !konowSwift for true retorna false, se for false, retorna true.
print("konowSwift result is:",konowSwift)


//Operador ternário

var grade = 7.95
var gradeResult = (grade >= 7.0) ? "Aprovado":"Reprovado"
print("gradeResult is":,gradeResult)

let number = 11
let type = (11 % 2 == 0) ? "Par":"Ímpar" // Se o módulo da divisão 11/2 for igual a zero, retorna par, se não retorna impar
print(type)

