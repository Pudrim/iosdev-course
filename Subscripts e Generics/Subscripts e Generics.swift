/*

Subscropts e Generics

*/

//SUBSCRIPTS

class Car {
  var licensePlate: String

  subscript(index: Int)-> String {
    //obter o valor do caracter conforme o index passado
    get {
      let character = Array(licensePlate)[index]
      return String(character)
    }
    //alterar o caracter conforme index fornecido
    set{
      var array = Array(licensePlate)
      array[index] = Character(newValue)
      self.licensePlate = String(array)
    }
  }

  init(licensePlate:String){
    self.licensePlate = licensePlate
  }
}

var car = Car(licensePlate:"EXP-8722")
print("O ultimo caracter da string informada é: \(car[7])")
print(car.licensePlate)
//Alterando a placa
car[0] = "A"
print(car.licensePlate)

/*
output ->

O ultimo caracter da string informada é: 2
EXP-8722
AXP-8722

*/


//GENERICS -> trocando os valores dos parêmetros passados na varivavel
func swapInts(_ a:inout Int, _ b: inout Int){
  let tempA = a
  a = b
  b = tempA
}

var a = 10
var b = 20
print(swapInts(&a, &b))

//passando qualquer tipo de variavel
func swapValues<T>(_ aa:inout T,_ bb: inout T){
  var tempAa = a
  aa = bb
  bb = tempAa
}

var aa = "10"
var bb = "20"
swapInts(&aa, &bb)
